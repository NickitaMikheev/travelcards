with cte as 
(
	select s.*,
		rank() over(partition by s.customerId order by s.customerId, s.datetime) as rnk
	from Sales s
)
select productId, count(productId) as cnt
	from cte
	where rnk = 1
	group by productId
;