﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TravelCards
{
	public class Card
	{
		public static IComparer DepartureComparer = new CardDepartureComparer();
		public static IComparer DestinationComparer = new CardDestinationComparer();

		public string Departure { get; }
		public string Destination { get; }

		public Card(String departure, String destination)
		{
			Departure = departure;
			Destination = destination;
		}

		#region Overrides

		public override string ToString()
		{
			return $"Departure: {Departure,10}, Destination: {Destination,10}";
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Departure?.GetHashCode() ?? 0) * 397) ^ (Destination?.GetHashCode() ?? 0);
			}
		}

		protected bool Equals(Card other)
		{
			return string.Equals(Departure, other.Departure) && string.Equals(Destination, other.Destination);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Card) obj);
		}

		#endregion

	}

	public class CardDestinationComparer : IComparer
	{
		public int Compare(object x, object y)
		{
			var cardX = x as Card;
			var cardY = y as Card;
			if (cardX != null && cardY != null)
				return String.CompareOrdinal(cardX.Destination, cardY.Destination);

			var stringX = x as String;
			var stringY = y as String;

			if (cardX != null && stringY != null)
				return String.CompareOrdinal(cardX.Destination, stringY);

			if (stringX != null && cardY != null)
				return String.CompareOrdinal(stringX, cardY.Destination);

			throw new InvalidOperationException();
		}
	}

	public class CardDepartureComparer : IComparer
	{
		public int Compare(object x, object y)
		{
			var cardX = x as Card;
			var cardY = y as Card;
			if (cardX != null && cardY != null)
				return String.CompareOrdinal(cardX.Departure, cardY.Departure);

			var stringX = x as String;
			var stringY = y as String;

			if (cardX != null && stringY != null)
				return String.CompareOrdinal(cardX.Departure, stringY);

			if (stringX != null && cardY != null)
				return String.CompareOrdinal(stringX, cardY.Departure);

			throw new InvalidOperationException();
		}
	}
}
