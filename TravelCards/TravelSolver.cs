﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TravelCards
{
	public class TravelSolver
	{
		public List<Card> Solve(List<Card> input)
		{
			var result = new List<Card>(input.Count);

			var cards = input.ToArray();
			Array.Sort(cards, Card.DepartureComparer);
			var departures   = new HashSet<String>(input.Select(x => x.Departure));
			var destinations = new HashSet<String>(input.Select(x => x.Destination));

			var first = departures.Where(x => !destinations.Contains(x)).ToList();
			if (first.Count != 1)
				throw Ex.GraphConstraint;

			var currentTown = first[0];
			do
			{
				var indx = Array.BinarySearch(cards, currentTown, Card.DepartureComparer);

				if (indx < 0)
					throw Ex.GraphConstraint;

				currentTown = cards[indx].Destination;
				result.Add(cards[indx]);
			} while (result.Count != input.Count);

			return result;
		}
	}

	public static class Ex
	{
		public static Exception GraphConstraint => new InvalidOperationException("Input cards should describe unilaterally connected graph without cycles");
	}

}
