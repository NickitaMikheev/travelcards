﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TravelCards.Tests
{
	[TestClass]
	public class BasiTests
	{
		[TestMethod]
		public void BasicTest()
		{
			var solver = new TravelSolver();

			var input = new List<Card>
			{
				new Card("Melbourne", "Cologne" ),
				new Card("Moscow"   , "Paris"   ),
				new Card("Cologne"  , "Moscow"  ),
			};

			var output = solver.Solve(input);

			AssertValidPath(output);
			Assert.AreEqual(input.Count, output.Count);
			Assert.AreEqual(input[0], output[0]);
			Assert.AreEqual(input[2], output[1]);
			Assert.AreEqual(input[1], output[2]);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void CycleTest()
		{
			var solver = new TravelSolver();

			var input = new List<Card>
			{
				new Card("A", "B"),
				new Card("B", "C"),
				new Card("C", "A"),
				new Card("A", "D"),
			};

			var output = solver.Solve(input);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void TwoArcComponentsTest()
		{
			var solver = new TravelSolver();

			var input = new List<Card>
			{
				new Card("A", "B"),
				new Card("C", "D"),
				new Card("D", "E"),
				new Card("E", "C"),
			};

			var output = solver.Solve(input);
		}

		[TestMethod]
		public void OneItemTest()
		{
			var solver = new TravelSolver();

			var input = new List<Card>
			{
				new Card("Melbourne", "Cologne" ),
			};

			var output = solver.Solve(input);

			AssertValidPath(output);
			Assert.AreEqual(input.Count, output.Count);
			Assert.AreEqual(input[0], output[0]);
		}

		[TestMethod]
		public void HugeForwardTest()
		{
			var max = 10000;
			var input = Enumerable.Range(0, max).Select(x => new Card(x.ToString(), (x + 1).ToString())).ToList();

			var solver = new TravelSolver();

			var output = solver.Solve(input);

			AssertValidPath(output);
			Assert.AreEqual(input.Count, output.Count);
			for (int i = 0; i < max; i++)
				Assert.AreEqual(input[i], output[i]);
		}

		[TestMethod]
		public void HugeBackwardsTest()
		{
			var max = 10000;
			var input = Enumerable.Range(0, max).Select(x => new Card((max - x).ToString(), (max - x + 1).ToString())).ToList();

			var solver = new TravelSolver();

			var output = solver.Solve(input);

			AssertValidPath(output);
			Assert.AreEqual(input.Count, output.Count);
			for (int i = 0; i < max; i++)
				Assert.AreEqual(input[max - i - 1], output[i]);
		}

		[TestMethod]
		public void HugeRandomTest()
		{
			var seed = new Guid().GetHashCode();

			var rnd = new Random(seed);
			var max = 10000;
			var input = Enumerable.Range(0, max).Select(x => new Card(x.ToString(), (x + 1).ToString())).OrderBy(x => rnd.Next()).ToList();

			var solver = new TravelSolver();
			var output = solver.Solve(input);

			var seedString = $"Seed: {seed}";
			AssertValidPath(output);
			Assert.AreEqual(input.Count, output.Count);
			for (int i = 0; i < max; i++)
			{
				Assert.AreEqual(i.ToString(), output[i].Departure, seedString);
				Assert.AreEqual((i+1).ToString(), output[i].Destination, seedString);
			}
		}

		private void AssertValidPath(List<Card> path)
		{
			for (int i = 0; i < path.Count - 1; i++)
				Assert.AreEqual(path[0].Destination, path[1].Departure);
		}
	}
}
